from mrr_calculations.models import *
import datetime

def update_client_mrr_year():
    qs = InvoiceItem.objects.all()
    
    for x in qs:
        new_invoice_date = x.invoice_date + datetime.timedelta(days=365)
        new_start_date = x.start_date + datetime.timedelta(days=365)
        new_end_date = x.end_date + datetime.timedelta(days=365)
        x.invoice_date = new_invoice_date
        x.start_date = new_start_date
        x.end_date = new_end_date
        x.save()
